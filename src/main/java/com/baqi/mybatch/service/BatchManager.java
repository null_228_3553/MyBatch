package com.baqi.mybatch.service;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.quartz.JobDataMap;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.baqi.mybatch.common.BatchTask;
import com.baqi.mybatch.common.JobProxy;
import com.baqi.mybatch.common.MyBatch;

@Service
public class BatchManager implements ApplicationContextAware{
	
	private Scheduler scheduler;
	
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		try {
			scheduler=new StdSchedulerFactory().getScheduler();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		init(applicationContext);
	}
	public void init(ApplicationContext context){
		try {
			int id=0;
			for(String beanName:context.getBeanDefinitionNames()){
				Object o=context.getBean(beanName);
				Class<?> c=o.getClass();
				//Cglib为动态代理生成的真实类的子类，无annotation，此处判断是否是Cglib代理类，若是，取父类
				if(c.getName().startsWith(c.getSuperclass().getName()+"$$EnhancerBySpringCGLIB$$")){
					c=c.getSuperclass();
				}
				for(Method m:c.getDeclaredMethods()){
					MyBatch myTask=m.getAnnotation(MyBatch.class);
					if(myTask==null){
						continue;
					}
					BatchTask task=new BatchTask();
					task.setId(id++);
					task.setGroup(c.getSimpleName());
					task.setName(m.getName());
					task.setObj(o);
					task.setMet(m);
					task.setCron(myTask.cron());
					add(task);
	
					JobDetailImpl jobDetail=new JobDetailImpl();
					jobDetail.setGroup(task.getGroup());//group名为类名，一个类一个组
					jobDetail.setName(task.getName());//方法名为job名
					jobDetail.setJobClass(JobProxy.class);
	
					CronTriggerImpl trigger=new CronTriggerImpl();
					trigger.setGroup(task.getGroup());
					trigger.setName(task.getName());
					trigger.getJobDataMap().put("task", task);
					trigger.setCronExpression(task.getCron());
	
					scheduler.scheduleJob(jobDetail, trigger);
				}
			}
			if(!scheduler.isShutdown()){
				scheduler.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*String taskPath="batch.task";
		try {
			List<File> fileList=scanPackage(taskPath);
			int id=0;
			for(File f:fileList){
				String fileName =  f.getAbsolutePath();
				//扫描class文件
				if(fileName.endsWith(".class")){
					String nosuffixFileName = fileName.substring(8+fileName.lastIndexOf("classes"),fileName.indexOf(".class"));
					String filePackage = nosuffixFileName.replaceAll("\\\\", ".");
					Class<?> c = Class.forName(filePackage);

					String className=filePackage.substring(filePackage.lastIndexOf(".")+1);
					className=className.substring(0,1).toLowerCase()+className.substring(1);
					Object o=context.getBean(className);
					for(Method m:c.getMethods()){
						MyBatch myTask=m.getAnnotation(MyBatch.class);
						if(myTask==null){
							continue;
						}
						BatchTask task=new BatchTask();
						task.setId(id++);
						task.setGroup(c.getSimpleName());
						task.setName(m.getName());
						task.setObj(o);
						task.setMet(m);
						task.setCron(myTask.cron());
						add(task);

						JobDetailImpl jobDetail=new JobDetailImpl();
						jobDetail.setGroup(task.getGroup());//group名为类名，一个类一个组
						jobDetail.setName(task.getName());//方法名为job名
						jobDetail.setJobClass(JobProxy.class);

						CronTriggerImpl trigger=new CronTriggerImpl();
						trigger.setGroup(task.getGroup());
						trigger.setName(task.getName());
						trigger.getJobDataMap().put("task", task);
						trigger.setCronExpression(task.getCron());

						scheduler.scheduleJob(jobDetail, trigger);
					}

				}
			}
			if(!scheduler.isShutdown()){
				scheduler.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
	}
	
	/*private List<File> scanPackage(String taskPath) {
		if(taskPath==null)
			taskPath="";
		taskPath=taskPath.replaceAll("\\.", "/");
		URL url=this.getClass().getClassLoader().getResource(taskPath+"/");
		File dir=new File(url.getFile());
		List<File> fileList = new ArrayList<File>();
		fetchFileList(dir,fileList);
		return fileList;
	}
	//递归读取
	private static void  fetchFileList(File dir,List<File> fileList){
		if(dir.isDirectory()){
			for(File f:dir.listFiles()){
				fetchFileList(f,fileList);
			}
		}else{
			fileList.add(dir);
		}
	}*/

	public Scheduler getScheduler() {
		return scheduler;  
	}
	
	private List<BatchTask> taskList = new LinkedList<BatchTask>();
	
	public List<BatchTask> getTaskList() {
		return taskList;
	}
	
	public void add(BatchTask task){
		taskList.add(task);
	}
	
	public BatchTask getTask(int id) {
		Iterator<BatchTask> i = taskList.iterator();
		while(i.hasNext()){
			BatchTask task=i.next();
			if(task.getId()==id){
				return task;
			}
		}
		return null;
	}
	
	public void runOnce(int id) throws SchedulerException{
		BatchTask task=getTask(id);
		JobKey jk=new JobKey(task.getName(),task.getGroup());
		JobDataMap jd=new JobDataMap();
		jd.put("task", task);
		scheduler.triggerJob(jk,jd);
	}
	
	public void pauseTask(int id) throws SchedulerException{
		BatchTask task=getTask(id);
		JobKey jk=new JobKey(task.getName(),task.getGroup());
		scheduler.pauseJob(jk);
		task.setState(BatchTask.stop);
	}
	
	public void resumeTask(int id) throws SchedulerException{
		BatchTask task=getTask(id);
		JobKey jk=new JobKey(task.getName(),task.getGroup());
		scheduler.resumeJob(jk);
		task.setState(BatchTask.run);
	}

	public void modifyTask(int id,String expr) throws ParseException, SchedulerException{
		BatchTask task=getTask(id);
		TriggerKey tk=new TriggerKey(task.getName(), task.getGroup());
		CronTriggerImpl trigger = (CronTriggerImpl)scheduler.getTrigger(tk);
		trigger.setCronExpression(expr);
		scheduler.rescheduleJob(tk, trigger);
		task.setCron(expr);
	}

}
