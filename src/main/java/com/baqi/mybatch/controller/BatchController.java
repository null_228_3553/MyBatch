package com.baqi.mybatch.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.baqi.mybatch.common.BatchTask;
import com.baqi.mybatch.service.BatchManager;

@Controller
@RequestMapping("/batch")
public class BatchController{
	private Logger log = LoggerFactory.getLogger(BatchController.class);
	@Autowired
	private BatchManager batchManager;
	
	@RequestMapping(value={"/**/*.jpg","/**/*.png","/**/*.gif"})
	public void jpg(HttpServletRequest request,HttpServletResponse response) throws IOException{
		String contextPath = request.getContextPath();
		String requestURI = request.getRequestURI();		
		String fileName = requestURI.substring(contextPath.length()+"/batch/".length());
		String filePath = resourcePath+fileName;
		
		byte[] bytes = readByteArrayFromResource(filePath);
		if (bytes == null) {
			response.sendError(404);
			return;
		}
		response.getOutputStream().write(bytes);
	}
	@RequestMapping(value={"/**/*.css","/**/*.js","/*.html"})
	public void text(HttpServletRequest request,HttpServletResponse response) throws IOException{
		String contextPath = request.getContextPath();
		String requestURI = request.getRequestURI();		
		String fileName = requestURI.substring(contextPath.length()+"/batch/".length());
		String filePath = resourcePath+fileName;
		log.info("read file from batch:"+filePath);
		
		String text = readFromResource(filePath);
		if(text==null){
			log.info("file not found:"+filePath);
			response.sendError(404);
			return;
		}
		if (filePath.endsWith(".html")) {
			response.setContentType("text/html;charset=utf-8");
		}else if (filePath.endsWith(".css")) {
			response.setContentType("text/css;charset=utf-8");
		} else if (filePath.endsWith(".js")) {
			response.setContentType("text/javascript;charset=utf-8");
		}
		response.getWriter().write(text);
	}
	
	@RequestMapping("/batchList.do")
	@ResponseBody
	public JSONObject batchList(HttpServletRequest request){
		List<BatchTask> list=batchManager.getTaskList();
		JSONObject jo=new JSONObject();
		jo.put("list", JSONObject.toJSON(list));
		return succJson(jo);
	}
	
	@RequestMapping("/pause.do")
	@ResponseBody
	public JSONObject pause(HttpServletRequest request){
		int id=Integer.valueOf(request.getParameter("id"));
		try {
			batchManager.pauseTask(id);
		} catch (Exception e) {
			return errJson(e.getMessage());
		}
		return succJson();
	}
	
	@RequestMapping("/resume.do")
	@ResponseBody
	public JSONObject resume(HttpServletRequest request){
		int id=Integer.valueOf(request.getParameter("id"));
		try {
			batchManager.resumeTask(id);
		} catch (Exception e) {
			return errJson(e.getMessage());
		}
		return succJson();
	}
	
	@RequestMapping("/runOnce.do")
	@ResponseBody
	public JSONObject runOnce(HttpServletRequest request){
		int id=Integer.valueOf(request.getParameter("id"));
		try {
			batchManager.runOnce(id);
		} catch (Exception e) {
			return errJson(e.getMessage());
		}
		return succJson();
	}
	
	@RequestMapping("/update.do")
	@ResponseBody
	public JSONObject update(HttpServletRequest request){
		int id=Integer.valueOf(request.getParameter("id"));
		String cron=request.getParameter("id");
		try {
			batchManager.modifyTask(id, cron);
		} catch (Exception e) {
			return errJson(e.getMessage());
		}
		return succJson();
	}
	
	private JSONObject errJson(String msg){
		JSONObject jo=new JSONObject();
		jo.put("retCode", "error");
		jo.put("retMsg", msg);
		return jo;
	}
	private JSONObject succJson(){
		JSONObject jo=new JSONObject();
		jo.put("retCode", "success");
		return jo;
	}
	
	private JSONObject succJson(JSONObject jo){
		jo.put("retCode", "success");
		return jo;
	}
	
	public final static int DEFAULT_BUFFER_SIZE = 1024 * 4;
	private String resourcePath="com/baqi/resource/html/";
	
	private String readFromResource(String resource) throws IOException {
		InputStream in = null;
		try {
			in = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
			if (in == null) {
				return null;
			}
			InputStreamReader reader = new InputStreamReader(in, "UTF-8");
			StringWriter writer = new StringWriter();
			char[] buffer = new char[DEFAULT_BUFFER_SIZE];
			int n = 0;
			while (-1 != (n = reader.read(buffer))) {
				writer.write(buffer, 0, n);
			}
			return writer.toString();
		} finally {
			if(in!=null)
				in.close();
		}
	}
	private byte[] readByteArrayFromResource(String resource) throws IOException {
		InputStream in = null;
		try {
			in = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
			if (in == null) {
				return null;
			}
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			copy(in, output);
			return output.toByteArray();
		} finally {
			in.close();
		}
	}
	private long copy(InputStream input, OutputStream output) throws IOException {
		final int EOF = -1;
		byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
		long count = 0;
		int n = 0;
		while (EOF != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			count += n;
		}
		return count;
	}
}
